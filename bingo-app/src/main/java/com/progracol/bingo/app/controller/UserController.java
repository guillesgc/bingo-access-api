package com.progracol.bingo.app.controller;

import com.progracol.bingo.common.constant.Header;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping(value = {(Route.USER)}, produces = {(MediaType.APPLICATION_JSON_VALUE)})
public class UserController {

    @Autowired
    private TokenService tokenService;

    /**
     * Allow to get user from access token
     * @param accessToken
     * @return UserResponse
     */
    @GetMapping(Route.GET_USER_FROM_TOKEN)
    public UserResponse getUserFromToken(@RequestHeader(Header.AUTHORIZATION) String accessToken) {
        return tokenService.getUserFromJwt(accessToken);
    }
}
