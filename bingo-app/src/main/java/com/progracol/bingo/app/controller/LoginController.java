package com.progracol.bingo.app.controller;

import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = {(Route.ACCESS_TOKEN)}, produces = {(MediaType.APPLICATION_JSON_VALUE)})
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * Allow to login and return access to token
     * @param loginRequest
     * @return access token to consume ms endpoints
     */
    @PostMapping
    public AccessTokenResponse login(@RequestBody @Valid LoginRequest loginRequest) {
        return loginService.login(loginRequest);
    }
}
