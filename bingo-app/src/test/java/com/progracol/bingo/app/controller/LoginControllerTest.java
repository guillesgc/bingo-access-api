package com.progracol.bingo.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.LoginService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = LoginController.class)
@OverrideAutoConfiguration(enabled = true)
public class LoginControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LoginService loginService;

    @Test
    public void getUserFromToken() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        LoginRequest loginRequest = Generators.getLoginRequest();
        String jsonRequest = objectMapper.writeValueAsString(loginRequest);

        AccessTokenResponse accessToken = Generators.getAccessTokenResponse();
        String jsonResponse = objectMapper.writeValueAsString(accessToken);
        String url = Route.ACCESS_TOKEN;

        Mockito.doReturn(accessToken).when(loginService).login(Mockito.any());

        mvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())

                .andExpect(
                        MockMvcResultMatchers.content().string(Matchers.containsString(jsonResponse))
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
