package com.progracol.bingo.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progracol.bingo.common.constant.Header;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.TokenService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(controllers = UserController.class)
@OverrideAutoConfiguration(enabled = true)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TokenService tokenService;

    @Test
    public void getUserFromToken() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        UserResponse userResponse = Generators.getUserResponse();
        String token = "token";
        String url = Route.USER.concat(Route.GET_USER_FROM_TOKEN);

        Mockito.doReturn(userResponse).when(tokenService).getUserFromJwt(token);
        mvc.perform(
            MockMvcRequestBuilders.get(url)
                    .header(Header.AUTHORIZATION,token)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(
                        MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponse)))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
