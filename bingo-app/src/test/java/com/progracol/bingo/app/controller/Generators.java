package com.progracol.bingo.app.controller;


import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.response.UserResponse;

public final class Generators {
    public static LoginRequest getLoginRequest(){
        return new LoginRequest("bingo", "$2y$10$KXkzSuXE7tvP.JLoHKUHK.iWNTsR.mL0NE8wtWCojUzZ2U2E1vQVm");
    }

    public static UserResponse getUserResponse(){
        UserResponse userResponse = new UserResponse();
        userResponse.setId("1");
        userResponse.setUsername("bingo");
        return userResponse;
    }

    public static AccessTokenResponse getAccessTokenResponse(){
        AccessTokenResponse access = new AccessTokenResponse();
        access.setToken("access");
        return access;
    }

}
