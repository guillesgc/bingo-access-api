package com.progracol.bingo.common.constant;

public class Header {
    public static final String AUTHORIZATION = "Authorization";
    public static final String PREFIX = "Bearer ";
    public static final String SECRET = "mySecretKey";
}
