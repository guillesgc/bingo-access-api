package com.progracol.bingo.common.mapper;

import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.UserEntity;

public class UserMapper {

    public static UserResponse userEntityToUserDto(UserEntity user) {
        UserResponse userDto = new UserResponse(user.getUserId().toString(), user.getNames(), user.getUsername(), user.getPassword());
        return userDto;
    }
}
