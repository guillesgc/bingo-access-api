package com.progracol.bingo.common.jwt;

import com.progracol.bingo.common.response.UserResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class JwtUtil {

    public static String tokenGenerator(UserResponse user) {
        String secretKey = "mySecretKey";
        String token = Jwts
                .builder()
                .setId("JWT")
                .setSubject(user.getUsername())
                .claim("user", user)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
