package com.progracol.bingo.common.route;

public interface Route {
    String HEALTH = "/health";
    String USER = "/user";
    String ACCESS_TOKEN = "/access-token";
    String GET_USER_FROM_TOKEN = "/from-token";
}
