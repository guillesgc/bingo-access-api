package com.progracol.bingo.core.service;


import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.UserEntity;
import org.mindrot.jbcrypt.BCrypt;

public final class Generators {
    public static LoginRequest getLoginRequest(){
        return new LoginRequest("bingo", "bingo");
    }

    public static UserResponse getUserResponse(){
        UserResponse userResponse = new UserResponse();
        userResponse.setId("1");
        userResponse.setUsername("bingo");
        userResponse.setPassword(BCrypt.hashpw("bingo",  BCrypt.gensalt()));
        return userResponse;
    }

    public static AccessTokenResponse getAccessTokenResponse(){
        AccessTokenResponse access = new AccessTokenResponse();
        access.setToken("access");
        return access;
    }

    public static UserEntity getUserEntity(){
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(1L);
        userEntity.setUsername("bingo");
        return userEntity;
    }

}
