package com.progracol.bingo.core.service;

import com.progracol.bingo.app.Application;
import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.response.UserResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(classes = Application.class)
@ExtendWith(SpringExtension.class)
public class LoginServiceTest {

    @InjectMocks
    private LoginService loginService;

    @Mock
    private UserService userService;

    @Mock
    private MessageSource messageSource;

    @Test
    public void login(){ ;
        LoginRequest loginRequest = Generators.getLoginRequest();
        UserResponse userResponse = Generators.getUserResponse();
        Mockito.doReturn(userResponse).when(userService).getUserByUsername(loginRequest.getUsername());
        AccessTokenResponse accessTokenResponse = loginService.login(loginRequest);

        Assertions.assertNotNull(accessTokenResponse);
        Assertions.assertNotNull(accessTokenResponse.getToken());
    }
}
