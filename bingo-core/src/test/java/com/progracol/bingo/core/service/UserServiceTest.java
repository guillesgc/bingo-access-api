package com.progracol.bingo.core.service;

import com.progracol.bingo.app.Application;
import com.progracol.bingo.common.constant.ErrorCode;
import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.UserEntity;
import com.progracol.bingo.domain.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(classes = Application.class)
@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private MessageSource messageSource;

    @Test
    public void getUserByUserName(){
        UserEntity userEntity = Generators.getUserEntity();
        Mockito.doReturn(userEntity).when(userRepository).findByUsername(Mockito.anyString());
        UserResponse userResponse = userService.getUserByUsername("bingo");

        Assertions.assertEquals(userEntity.getUserId().toString(), userResponse.getId());
        Assertions.assertEquals(userEntity.getUsername(), userResponse.getUsername());
    }

    @Test
    public void getUserByUserNameReturnException(){

        BingoException exception = Assertions.assertThrows(BingoException.class, () -> {
            Mockito.doReturn(null).when(userRepository).findByUsername(Mockito.anyString());
            UserResponse userResponse = userService.getUserByUsername("bingo");
        });

        Assertions.assertEquals(ErrorCode.INVALID_USER_OR_PASSWORD, exception.getErrorCode());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), exception.getStatus());
    }
}
