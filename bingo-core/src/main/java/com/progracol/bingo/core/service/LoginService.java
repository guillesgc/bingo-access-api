package com.progracol.bingo.core.service;

import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.jwt.JwtUtil;
import com.progracol.bingo.common.request.LoginRequest;
import com.progracol.bingo.common.response.AccessTokenResponse;
import com.progracol.bingo.common.response.UserResponse;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static com.progracol.bingo.common.constant.ErrorCode.INVALID_USER_OR_PASSWORD;

@Service
public class LoginService {

    private Logger logger = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    /**
     * To make login
     * @param login
     * @return if the login was successful return AccessTokenResponse otherwise throw BingoException
     */
    public AccessTokenResponse login(LoginRequest login) {
        UserResponse user = userService.getUserByUsername(login.getUsername());
        validatePassword(login, user);
        user.setPassword(null);
        AccessTokenResponse accessToken = new AccessTokenResponse();
        String token = JwtUtil.tokenGenerator(user);
        accessToken.setToken(token);
        return accessToken;
    }

    /**
     * Check if password is a Bcrypt password
     * @param login
     * @param user
     */
    private void validatePassword(LoginRequest login, UserResponse user) {
        Boolean isValid = BCrypt.checkpw(login.getPassword(), user.getPassword());
        if (isValid == null || !isValid) {
            String message = messageSource.getMessage(INVALID_USER_OR_PASSWORD, null, LocaleContextHolder.getLocale());
            throw new BingoException(HttpStatus.BAD_REQUEST.value(), INVALID_USER_OR_PASSWORD, message);
        }
    }
}
