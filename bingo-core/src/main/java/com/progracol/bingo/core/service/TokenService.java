package com.progracol.bingo.core.service;


import com.progracol.bingo.common.response.UserResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.progracol.bingo.common.constant.Header.PREFIX;
import static com.progracol.bingo.common.constant.Header.SECRET;

@Service
public class TokenService {

    private Logger logger = LoggerFactory.getLogger(TokenService.class);

    /**
     * This method validate if the jwt is valid and return UserResponse
     * @param jwtToken
     * @return UserResponse
     */
    public UserResponse getUserFromJwt(String jwtToken) {
        UserResponse user = null;
        try {
            if (existJwtToken(jwtToken)) {
                Claims claims = validateToken(jwtToken);
                if (claims.get("user") != null) {
                    Map use = (Map) claims.get("user");
                    String username = (String) use.get("username");
                    String id = (String) use.get("id");
                    user = new UserResponse(id, username);
                }
            }
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException e) {
            logger.error("TokenService::isValidJwt --ERROR [{}}", e.getMessage());
        } catch (Exception e) {
            logger.error("TokenService::isValidJwt --ERROR [{}}", e.getMessage());
        }
        return user;
    }

    /**
     * This method validate if the jwt is valid
     * @param jwtToken
     * @return Claims
     */
    private Claims validateToken(String jwtToken) {
        jwtToken = jwtToken.replace(PREFIX, "");
        return Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    /**
     * Check if the token contain the prefix
     * @param jwtToken
     * @return
     */
    private boolean existJwtToken(String jwtToken) {
        if (jwtToken == null || !jwtToken.startsWith(PREFIX))
            return false;
        return true;
    }
}
