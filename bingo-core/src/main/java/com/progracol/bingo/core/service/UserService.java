package com.progracol.bingo.core.service;

import static com.progracol.bingo.common.constant.ErrorCode.INVALID_USER_OR_PASSWORD;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.mapper.UserMapper;
import com.progracol.bingo.domain.entity.UserEntity;
import com.progracol.bingo.domain.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    /**
     * Get user by username from db
     * @param username
     * @return UserResponse
     */
    public UserResponse getUserByUsername(String username) {
        UserResponse userDto = null;
        UserEntity user = userRepository.findByUsername(username);
        if(Objects.nonNull(user)) {
            userDto = UserMapper.userEntityToUserDto(user);
        }else {
            String message = messageSource.getMessage(INVALID_USER_OR_PASSWORD, null, LocaleContextHolder.getLocale());
            throw new BingoException(HttpStatus.BAD_REQUEST.value(), INVALID_USER_OR_PASSWORD, message);
        }
        logger.info("UserService::findByUsername --names [{}] --userId [{}]", userDto.getNames(),  userDto.getId());
        return userDto;
    }
}
